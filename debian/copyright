Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Debianized-By: Boris Pek <tehnick-8@yandex.ru>
Debianized-Date: Thu, 11 Feb 2021 00:37:07 +0300
Upstream-Name: Official plugins for Psi
Upstream-Contact: Boris Pek <tehnick-8@yandex.ru>
Source: https://github.com/psi-im/plugins/tags
 The orig.tar.gz is just renamed upstream tarball (without changes).

Files: *
Copyright: 2003-2020 Psi and Psi+ developers
License: LGPL-2.1+

Files: debian/*
Copyright: 2021 Boris Pek <tehnick-8@yandex.ru>
License: LGPL-2.1+

Files: deprecated/*plugin/*
       generic/*plugin/*
Copyright: 2009-2013 Sergey Ilinyh aka Rion <rion4ik@gmail.com>
           2009-2012 Evgeny Khryukin
           2009-2010 Virnik
           2006-2007 Maciej Niedzielski
           2009-2012 Kravtsov Nikolai
           2009-2010 VampiRus
           2010-2017 Ivan Romanov aka taurus <drizt@land.ru>
           2011-2017 Aleksey Andreev
           2003 Justin Karneges <justin@karneges.com>
           2010 senu
           2008 Ustyugov Roman <qtdevel@gmail.com>
           2010 Michail Pishchagin <mblsha@users.sourceforge.net>
           2010 Dmitriy.trt
           2008 Armando Jagucki
           2010 Vitaly Tonkacheyev aka KukuRuzo
           2004-2008 Ian Goldberg
           2004-2008 Chris Alexander
           2004-2008 Nikita Borisov
           2007-2011 Timo Engel <timo-e@freenet.de>
           2008-2009 Alexander Kazarin <boiler@co.ru>
           2013-2020 Boris Pek <tehnick-8@yandex.ru>
           2013 Georg Rudoy
           2016 rkfg
License: GPL-2+

Files: generic/screenshotplugin/qxt/*
Copyright: 2006-2011 Qxt Foundation <foundation@libqxt.org>
           2011 Marcin Jakubowski <mjakubowski@gmail.com>
License: BSD-3-clause

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 2.1 of the License, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
